## Common password lookup

How to use

`echo 'alias checkpassword=ruby\ ~/path/to/blue-42/bin/main.rb' >> ~/.bash_profile`

checkpassword foo
=> Not common password
checkpassword football
=> common password
