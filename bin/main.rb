require 'rubygems'
require 'bundler/setup'

require 'pry'
require 'logger'

Dir['./lib/**/*.rb'].sort.each { |file| require file }

logger = Logger.new(STDOUT)
logger.level = ENV.fetch('LOG_LEVEL') { :debug }.to_sym

FILE_LOCATION = 'vendor/data/common_passwords.txt'

# Handle exit
at_exit do
  # logger.debug { 'Password lookup completed.' }
end

# Output to STDOUT
begin
  file = File.open(FILE_LOCATION)
  password = Password.new(file, logger: logger)
  password.parse_passwords
  output = password.assess_commonality
  $stdout.puts(output)

rescue StandardError => e
  logger.error(error: 'An unexpected error occurred', message: e.message, backtrace: e.backtrace)
end
