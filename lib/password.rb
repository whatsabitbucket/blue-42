class Password

  attr_reader :file, :logger
  attr_accessor :common_passwords

  def initialize(file, logger: Logger.new(STDOUT))
    @file = file
    @logger = logger
    @common_passwords ||= {}
  end

  # @param file [File] file to parse
  # @returh [Hash]
  def parse_passwords
    file.each do |row|
      password = parse_password(row)
      next if password.nil? || password.empty?
      common_passwords.store(password, 'Common password')
    end
    common_passwords
  end

  def assess_commonality
    if common_passwords[ARGV[0]]
      'Common password'
    else
      'Not common password'
    end
  end

  private

  # @param row [String]
  # @return [String]
  def parse_password(row)
    password = row.strip!
  end
end
